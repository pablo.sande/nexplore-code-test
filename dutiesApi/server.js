const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Duties = require('./api/models/dutiesModel'); 

const cors = require('cors'); 
  
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Dutydb', { useNewUrlParser: true }); 

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

var routes = require('./api/routes/dutiesRoutes'); 
routes(app); 

app.listen(port);

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

console.log('todo list RESTful API server started on: ' + port);