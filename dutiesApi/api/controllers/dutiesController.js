'use strict';


var mongoose = require('mongoose'),
  duty = mongoose.model('Duties');

exports.list_all_duties = function(req, res) {
  duty.find({}, function(err, duty) {
    if (err)
      res.send(err);
    res.json(duty);
  });
};

exports.create_a_duty = function(req, res) {
  var new_duty = new duty(req.body);
  new_duty.save(function(err, duty) {
    if (err)
      res.send(err);
    res.json(duty);
  });
};

exports.update_a_duty = function(req, res) {
  duty.findOneAndUpdate({_id: req.params.dutyId}, req.body, {new: true}, function(err, duty) {
    if (err)
      res.send(err);
    res.json(duty);
  });
};

exports.delete_a_duty = function(req, res) {
  duty.remove({
    _id: req.params.dutyId
  }, function(err, duty) {
    if (err)
      res.send(err);
    res.json({ message: 'Duty successfully deleted' });
  });
};
