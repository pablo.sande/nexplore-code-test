'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var DutySchema = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the duty'
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Duties', DutySchema);