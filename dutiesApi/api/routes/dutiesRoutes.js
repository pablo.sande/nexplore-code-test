'use strict';
module.exports = function(app) {
  var duties = require('../controllers/dutiesController');

  // duties Routes
  app.route('/duties')
    .get(duties.list_all_duties)
    .post(duties.create_a_duty);


  app.route('/duties/:dutyId')
    .put(duties.update_a_duty)
    .delete(duties.delete_a_duty);
};
