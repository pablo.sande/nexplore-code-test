# Instructions

We are assuming that you have [Node.js](https://nodejs.org/en/download/) and [MongoDB](https://docs.mongodb.com/manual/installation/) installed.

## Set up the Backend

Install the dependencies:

`cd dutiesApi`
`npm i`

Start MongoDB Server:

`mongod`

Run the API (listening on `http://localhost:3000/`):

`npm run start`

## Set up the SPA

Open a new terminal/tab in the root folder. Install the dependencies:

`cd dutiesSpa`
`npm i`

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
