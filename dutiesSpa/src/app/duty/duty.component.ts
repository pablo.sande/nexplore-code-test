import { Component, OnInit } from '@angular/core';
import { Duty } from './model/duty';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { DutyService } from '../duty.service';


@Component({
  selector: 'app-duty',
  templateUrl: './duty.component.html',
  styleUrls: ['./duty.component.scss'],
})

export class DutyComponent implements OnInit {
  dutyForm: FormGroup = this.fb.group({
    dutyNames: this.fb.array([])
  });
  duties: Duty[] = [];
  readonly MAX_NAME_LENGTH = 20;
  
  constructor(private fb: FormBuilder, private dutyService: DutyService) { }
  
  get dutyNames() {
    return this.dutyForm.get('dutyNames') as FormArray
  }

  ngOnInit(): void {
    this.dutyService
      .getDuties()
      .subscribe(duties => {
        this.duties = duties;
        this.dutyNames.controls = duties
        .map((duty: Duty) => this.fb.control(duty.name, [Validators.required, Validators.maxLength(this.MAX_NAME_LENGTH)]))
      })
  }

  addDuty() {
    this.dutyNames.push(this.fb.control('', [Validators.required, Validators.maxLength(this.MAX_NAME_LENGTH)]));
  }

  deleteDuty(i: number) {
    if (this.duties[i] && this.duties[i]._id) {
      this.dutyService
      .deleteElement(this.duties[i]._id)
      .subscribe(() => {
        this.duties.splice(i, 1);
        this.dutyNames.controls.splice(i, 1);
      })
    } else {
      this.dutyNames.controls.splice(i, 1);
    }
  }

  onSubmit() {
    this.dutyNames.controls.forEach((item, i) => {
      if (item.touched) {
        this.duties[i] && this.duties[i]._id ?
          this.dutyService
          .updateDuty(item.value, this.duties[i]._id)
          .subscribe((d: Duty) => {
            item.markAsUntouched();
            this.duties[this.duties.findIndex(_ => _._id === d._id)] = d;
          })
        : this.dutyService
          .saveDuty(item.value)
          .subscribe(d => {
            if (d && d._id) {
              this.duties.push(d)
              item.markAsUntouched()
            }
          })
      } 
    })
  }
}
