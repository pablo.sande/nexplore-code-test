import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DutyComponent } from './duty.component';
import { Duty } from './model/duty';
import { DutyService } from '../duty.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormBuilder, FormArray, FormGroup, Validators, ReactiveFormsModule, FormControl } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';


describe('DutyComponent', () => {
  let component: DutyComponent;
  let fixture: ComponentFixture<DutyComponent>;
  let httpMock: HttpTestingController;
  const mockList: Duty[] = [
    {
      name: 'test1',
      _id: '1'
    },
    {
      name: 'test2',
      _id: '2'
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DutyComponent ],
      providers: [
        { provide: DutyService, useValue: {
          getDuties: () => of(mockList),
          deleteElement: () => of(true),
          updateDuty: (name: string, _id: string) => of({name, _id}),
          saveDuty: (name: string) => of({name, _id: '3'})
        }},
        FormBuilder,
        Validators,
      ],
      imports: [
        MatInputModule,
        MatFormFieldModule,
        HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();

    httpMock = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DutyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the list ', () => {
    component.ngOnInit();
    expect(component.duties).toEqual(mockList)
  });

  it('should delete the item', () => {
    component.ngOnInit();
    component.deleteDuty(1);
    expect(component.duties).toEqual([mockList[0]]);
  })

  it('should add a new issue', () => {
    component.ngOnInit();
    component.dutyNames.controls = component.duties
      .map((duty: Duty) => new FormControl(duty.name))
    
    component.dutyNames.controls.push(new FormControl('test3'));
    component.dutyNames.controls[component.dutyNames.controls.length-1].markAsTouched()
    component.onSubmit();
    expect(component.duties.pop()).toEqual({name: 'test3', _id: '3'})
  })

  it('should update an issue', () => {
    component.ngOnInit();
    component.dutyNames.controls = component.duties
      .map((duty: Duty) => new FormControl(duty.name))
    
    component.dutyNames.controls[0].setValue('test4')
    component.dutyNames.controls[0].markAsTouched()
    component.onSubmit();
    expect(component.duties[0]).toEqual({name: 'test4', _id: '1'})
  })
});
