import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DutyComponent } from './duty/duty.component';

const routes: Routes = [
  { path: '', redirectTo: '/duties', pathMatch: 'full'},
  { path: 'duties', component: DutyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
