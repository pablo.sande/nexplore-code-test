import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DutyService {
  private configUrl = 'http://localhost:3000/duties';

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  public getDuties(): Observable<any> {
    return this.http.get(this.configUrl)
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }

  public saveDuty(dutyName: string): Observable<any> {
    return this.http.post(this.configUrl, {name: dutyName})
    .pipe(
      catchError(this.handleError)
    )
  }

  public updateDuty(dutyName: string, dutyId): Observable<any> {
    return this.http.put(`${this.configUrl}/${dutyId}`, {name: dutyName, id: dutyId})
    .pipe(
      catchError(this.handleError)
    )
  }

  public deleteElement(dutyId): Observable<any> {
    return this.http.delete(`${this.configUrl}/${dutyId}`)
    .pipe(
      catchError(this.handleError)
    )
  }
}
